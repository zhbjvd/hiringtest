﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using System.IO.Pipes;
using System.Threading;
using System.Security.Principal;
using System.Diagnostics;


namespace InterProcessCommunicationTest
{
    public class PipeClientTest
    {

        public NamedPipeClientStream pipeClient = new NamedPipeClientStream(".", "testpipe", PipeDirection.InOut, PipeOptions.Asynchronous);
        public StreamString ss;
        Thread readThread;

        // Use this for initialization
        public void Connect()
        {

            readThread = new Thread(ConnectInThread);
            readThread.Start();
        }

        void ConnectInThread()
        {
            Console.WriteLine("Pipe Opening Process Started");

            Console.WriteLine("Connecting to server...\n");
            pipeClient.Connect();
            StreamString ss = new StreamString(pipeClient);

            Console.WriteLine(ss.ReadString());

            Thread.Sleep(250);
        }

        // Update is called once per frame
        void Update()
        {
            getPipedData();
        }

        private void getPipedData()
        {
            //UnityEngine.Debug.Log("Thread Called - Start");

            StreamString ss = new StreamString(pipeClient);
            Console.WriteLine(ss.ReadString());
            //UnityEngine.Debug.Log("Thread Called - End");

        }
    }



    public class StreamString
    {
        private Stream ioStream;
        private UnicodeEncoding streamEncoding;

        public StreamString(Stream ioStream)
        {
            this.ioStream = ioStream;
            streamEncoding = new UnicodeEncoding();
        }

        public string ReadString()
        {
            int len;
            len = ioStream.ReadByte() * 256;
            len += ioStream.ReadByte();
            byte[] inBuffer = new byte[len];
            ioStream.Read(inBuffer, 0, len);

            string outString = streamEncoding.GetString(inBuffer);

            //ioStream.Flush();

            return outString;


        }

        public int WriteString(string outString)
        {
            byte[] outBuffer = streamEncoding.GetBytes(outString);
            int len = outBuffer.Length;
            if (len > UInt16.MaxValue)
            {
                len = (int)UInt16.MaxValue;
            }
            ioStream.WriteByte((byte)(len / 256));
            ioStream.WriteByte((byte)(len & 255));
            ioStream.Write(outBuffer, 0, len);
            ioStream.Flush();

            return outBuffer.Length + 2;
        }




    }
}
