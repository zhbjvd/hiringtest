﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace IPC
{
    public enum ChatSystemType
    {
        TCPIP,
        Pipe
    }

    [Serializable]
    public class ChatSystem
    {
        private ChatSystemType currentChatSystemType = ChatSystemType.TCPIP;
        private bool isServer = true;

        IChatSystemConnection chatClient;

        public bool IsConnectedToServer
        {
            get
            {
                return chatClient != null ? chatClient.IsConnected() : false;
            }
        }

        public ChatConnectionStatus GetChatConnectionStatus
        {
            get
            {
                if(chatClient != null)
                {
                    return chatClient.ConnectionStatus;
                }

                return ChatConnectionStatus.Disconnected;
            }
        }

        public Action<ChatMessage> messageReceivedE;
        public Action<ChatMessage> onChatMessageSentE;
        public Action<ChatConnectionStatus> OnConnectionStatusUpdateE;
        public ChatSystem(bool isServer)
        {
            this.isServer = isServer;
        }

        public void Connect()
        {
            chatClient = new TCPChatClient();
            chatClient.Connect();

            chatClient.SetOnMessageReceivedDelegate(OnMessageReceived);
            chatClient.SetConnectionStatusUpdateDelegate(OnConnectionStatusUpdate);
        }

        public void Disconnect()
        {
            chatClient.Disconnect();
            chatClient.RemoveOnMessageReceivedDelegate(OnMessageReceived);
            chatClient.RemoveConnectionStatusUpdateDelegate(OnConnectionStatusUpdate);
        }

        public void SendMessage(string message)
        {
            if(chatClient != null)
            {
                var chatMessage = new ChatMessage(message, DateTime.Now, true);
                if (onChatMessageSentE != null)
                {
                    onChatMessageSentE(chatMessage);
                }

                chatClient.SendMessage(chatMessage.chatMessage);
            }
        }

        void OnConnectionStatusUpdate(ChatConnectionStatus status)
        {
            if(OnConnectionStatusUpdateE != null)
            {
                OnConnectionStatusUpdateE(status);
            }
        }

        void OnMessageReceived(string message)
        {
            var chatMessage = new ChatMessage(message, DateTime.Now, false);

            if (messageReceivedE != null)
            {
                messageReceivedE(chatMessage);
            }
        }

    }
}
