﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace IPC
{
    public class TCPChatClient : IChatSystemConnection
    {
        private ChatConnectionStatus connectionStatus = ChatConnectionStatus.Idle;

        public ChatConnectionStatus ConnectionStatus
        {
            get
            {
                return connectionStatus;
            }
        }

        #region private members 	
        private TcpClient socketConnection;
        private Thread clientReceiveThread;

        private Action<string> OnMessageReceivedE;
        private Action<ChatConnectionStatus> OnConnectionStatusE;

        #endregion

        public void Connect()
        {
            try
            {
                SetConnectionStatus(ChatConnectionStatus.Connecting);

                clientReceiveThread = new Thread(new ThreadStart(ListenForData));
                clientReceiveThread.IsBackground = true;
                clientReceiveThread.Start();
                
            }
            catch (Exception e)
            {
                Console.WriteLine("On client connect exception " + e);
            }
        }

        private void ListenForData()
        {
            try
            {
                socketConnection = new TcpClient("localhost", 8052);

                Console.WriteLine("TCPChatClient : ListenForData : Connected : " + socketConnection.Connected);

                SetConnectionStatus(ChatConnectionStatus.Connected);

                Byte[] bytes = new Byte[1024];
                while (true)
                {
                    // Get a stream object for reading 				
                    using (NetworkStream stream = socketConnection.GetStream())
                    {
                        int length;
                        // Read incomming stream into byte arrary. 					
                        while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            var incommingData = new byte[length];
                            Array.Copy(bytes, 0, incommingData, 0, length);
                            // Convert byte array to string message. 						
                            string serverMessage = Encoding.ASCII.GetString(incommingData);
                            Console.WriteLine("server message received as: " + serverMessage);

                            if(OnMessageReceivedE != null)
                            {
                                OnMessageReceivedE(serverMessage);
                            }
                        }
                    }
                }
            }
            catch (SocketException socketException)
            {
                Console.WriteLine("Socket exception: " + socketException);

                SetConnectionStatus(ChatConnectionStatus.Disconnected);
            }
            catch(Exception e)
            {
                Console.WriteLine("exception: " + e.Message);

                SetConnectionStatus(ChatConnectionStatus.Disconnected);
            }
        }

        public void Disconnect()
        {
            if (socketConnection != null && socketConnection.Client != null)
            {
                socketConnection.Client.Close();
            }

            if(clientReceiveThread != null)
            {
                clientReceiveThread.Abort();
            }

            SetConnectionStatus(ChatConnectionStatus.Disconnected);
        }

        public void SendMessage(string message)
        {
            if (socketConnection == null && !IsConnected())
            {
                return;
            }
            try
            {
                // Get a stream object for writing. 			
                NetworkStream stream = socketConnection.GetStream();
                if (stream.CanWrite)
                {
                    // Convert string message to byte array.                 
                    byte[] clientMessageAsByteArray = Encoding.ASCII.GetBytes(message);
                    // Write byte array to socketConnection stream.                 
                    stream.Write(clientMessageAsByteArray, 0, clientMessageAsByteArray.Length);
                    Console.WriteLine("TCPChatClient : SendMessage : " + message);
                }
            }
            catch (SocketException socketException)
            {
                Console.WriteLine("Socket exception: " + socketException);
            }
        }

        public void SetOnMessageReceivedDelegate(Action<string> eventReciever)
        {
            OnMessageReceivedE += eventReciever;
        }

        public void RemoveOnMessageReceivedDelegate(Action<string> eventReciever)
        {
            OnMessageReceivedE -= eventReciever;
        }

        public bool IsConnected()
        {
            if (socketConnection != null)
            {
                return socketConnection.Connected;
            }

            return false;
        }

        void SetConnectionStatus(ChatConnectionStatus status)
        {
            connectionStatus = status;

            if(OnConnectionStatusE != null)
            {
                OnConnectionStatusE(ConnectionStatus);
            }
        }

        public void SetConnectionStatusUpdateDelegate(Action<ChatConnectionStatus> updatedStatus)
        {
            OnConnectionStatusE += updatedStatus;
        }

        public void RemoveConnectionStatusUpdateDelegate(Action<ChatConnectionStatus> updatedStatus)
        {
            OnConnectionStatusE -= updatedStatus;
        }
    }
}
