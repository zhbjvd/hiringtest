﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using IPC;
namespace InterProcessCommunicationTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ChatSystem chatSystem;
        public MainWindow()
        {
            InitializeComponent();

            chatSystem = new ChatSystem(false);
            chatSystem.messageReceivedE += OnMessageAdded;
            chatSystem.onChatMessageSentE += OnMessageAdded;

            chatSystem.OnConnectionStatusUpdateE += OnConnectionStatusUpdate;

            connectionState.Content = ChatConnectionStatus.Disconnected;
        }

        void OnConnectionStatusUpdate(ChatConnectionStatus status)
        {
            this.Dispatcher.Invoke(() =>
            {
                connectionState.Content = status.ToString();
            });
        }

        void OnMessageAdded(ChatMessage messageAdded)
        {
            this.Dispatcher.Invoke(() =>
            {
                chatDisplayArea.Items.Add(messageAdded.ToString());
            });
            
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            if(chatSystem.GetChatConnectionStatus == ChatConnectionStatus.Disconnected ||
                chatSystem.GetChatConnectionStatus == ChatConnectionStatus.Idle)
            {
                chatSystem.Connect();
            }
            else
            {
                MessageBox.Show("Client already connected, disconnect before connecting again");
            }
        }

        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            if (chatSystem.GetChatConnectionStatus == ChatConnectionStatus.Connected)
            {
                chatSystem.Disconnect();
            }
            else
            {
                MessageBox.Show("Client already disconnected");
            }
        }
        
        private void sendButton_Click(object sender, RoutedEventArgs e)
        {
            if(chatSystem != null && chatSystem.IsConnectedToServer)
            {
                if (!string.IsNullOrEmpty(chatMessageInputArea.Text))
                {
                    chatSystem.SendMessage(chatMessageInputArea.Text);
                    chatMessageInputArea.Text = string.Empty;
                }
                else
                {
                    MessageBox.Show("Please write some message before sending");
                }
            }
            else
            {
                MessageBox.Show("Please connect to server before sending message");
            }
            
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void chatDisplayArea_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
