﻿using System.Collections;
using System.Collections.Generic;

namespace IPC
{
    public enum ChatConnectionStatus
    {
        Idle,
        Connecting,
        Connected,
        Disconnecting,
        Disconnected
    }
    public interface IChatSystemConnection
    {
        void Connect();
        void Disconnect();
        void SendMessage(string message);
        bool IsConnected();
        
        void SetOnMessageReceivedDelegate(System.Action<string> eventReciever);
        void RemoveOnMessageReceivedDelegate(System.Action<string> eventReciever);
        void SetConnectionStatusUpdateDelegate(System.Action<ChatConnectionStatus> updatedStatus);
        void RemoveConnectionStatusUpdateDelegate(System.Action<ChatConnectionStatus> updatedStatus);

        ChatConnectionStatus ConnectionStatus
        {
            get;
        }
    }
}
