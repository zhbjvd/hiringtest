﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Preview3D
{
    public class SpawnedItemToggler : MonoBehaviour
    {
        public PreviewType type;

        public void OnValueChange(bool isOn)
        {
            if(isOn)
            {
                Preview3DHandler.Instance.SetItem(type);
            }
        }
    }
}
