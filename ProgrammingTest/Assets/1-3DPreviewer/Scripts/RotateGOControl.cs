﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Preview3D
{
    public class RotateGOControl : MonoBehaviour
    {
        public float rotationSpeed = 10;

        // Use this for initialization
        void Start()
        {
            InputManager.MeshRotateMouseControlInputE += HandleMeshRotateMouseControlInput;
        }

        void HandleMeshRotateMouseControlInput(float mouseAxisX,float mouseAxisY)
        {
            transform.RotateAround(transform.position, Vector3.up, mouseAxisX * rotationSpeed);
            transform.RotateAround(transform.position, Vector3.right, mouseAxisY * rotationSpeed);
        }

        private void OnDestroy()
        {
            InputManager.MeshRotateMouseControlInputE -= HandleMeshRotateMouseControlInput;
        }
    }
}
