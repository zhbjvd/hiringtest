﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Preview3D
{
    public class CameraZoomControl : MonoBehaviour
    {
        public float minCameraZoom = -100;
        public float maxCameraZoom = 100;

        public float zoomSpeed = 100;
        private float currentZoomPercent = 0;
        // Use this for initialization
        void Start()
        {
            ResetCamera();
            InputManager.CameraZoomMouseControlInputE += HandleCameraZoomControlInput;
        }

        void HandleCameraZoomControlInput(float mouseY)
        {
            SetCameraZoom(currentZoomPercent + mouseY * zoomSpeed * Time.deltaTime);             
        }

        void SetCameraZoom(float percent)
        {
            if(percent > 100)
            {
                currentZoomPercent = 100 ;
            }
            else if(percent < 0)
            {
                currentZoomPercent = 0;
            }
            else
            {
                currentZoomPercent = percent;
            }

            float updatedPos = Mathf.Lerp(minCameraZoom, maxCameraZoom, percent / 100);

            Debug.Log("UpdatedPos : " + updatedPos + " : currentZoomPercent : " + currentZoomPercent);

            transform.position = new Vector3(transform.position.x, transform.position.y, updatedPos);
        }

        public void ResetCamera()
        {
            SetCameraZoom(0);            
        }

        private void OnDestroy()
        {
            InputManager.CameraZoomMouseControlInputE -= HandleCameraZoomControlInput;
        }
    }
}