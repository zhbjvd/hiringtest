﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Preview3D
{
    public class LightIntensitySlider : MonoBehaviour
    {
        public Slider sliderRef;

        private void Start()
        {
            sliderRef.value = Preview3DHandler.Instance.lightRef.intensity;
        }

        public void OnSliderValueUpdate(float updatedValue)
        {
            Preview3DHandler.Instance.SetLightIntensity(updatedValue);
        }
    }
}
