﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Preview3D
{
    public class InputManager : MonoBehaviour
    {
        public static Action<float> CameraZoomMouseControlInputE;
        public static Action<float,float> MeshRotateMouseControlInputE;
        public static Action<float, float> LightRotateMouseControlInputE;
        public static Action<GameObject> On3DObjectClickedE;

        public bool showDebug = false;

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
            {
                if (Input.GetMouseButton(0))
                {
                    if (showDebug)
                    {
                        Debug.Log("InputManager : Update : MeshRotateMouseControlInputE");
                    }

                    if (MeshRotateMouseControlInputE != null)
                    {
                        MeshRotateMouseControlInputE(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
                    }
                }
                else if (Input.GetMouseButton(1))
                {
                    if (showDebug)
                    {
                        Debug.Log("InputManager : Update : CameraZoomMouseControlInputE");
                    }

                    if (CameraZoomMouseControlInputE != null)
                    {
                        CameraZoomMouseControlInputE(Input.GetAxis("Mouse Y"));
                    }
                }
            }
            else if (Input.GetKey(KeyCode.LeftAlt) || Input.GetKey(KeyCode.RightAlt))
            {
                if (Input.GetMouseButton(0))
                {
                    if (showDebug)
                    {
                        Debug.Log("InputManager : Update : LightRotateMouseControlInputE");
                    }

                    if (LightRotateMouseControlInputE != null)
                    {
                        LightRotateMouseControlInputE(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
                    }
                }
            }
            else
            {
                if (Input.GetMouseButtonDown(0))
                {
                    var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (hit.collider != null)
                        {
                            if (On3DObjectClickedE != null)
                            {
                                On3DObjectClickedE(hit.collider.gameObject);
                            }
                        }
                    }
                }
            }
        }
    }
}

