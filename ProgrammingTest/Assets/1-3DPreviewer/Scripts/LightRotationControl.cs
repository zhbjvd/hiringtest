﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Preview3D
{
    public class LightRotationControl : MonoBehaviour
    {
        public float rotationSpeed = 10;
        public Transform target;

        // Use this for initialization
        void Awake()
        {
            Preview3DHandler.SpawnedObjectUpdateE += HandleOnSpawnedObjetUpdate;
            InputManager.LightRotateMouseControlInputE += HandleLightRotateMouseControlInputE;
        }

        void HandleLightRotateMouseControlInputE(float mouseAxisX, float mouseAxisY)
        {
            if(target != null)
            {
                transform.RotateAround(target.position, Vector3.up, mouseAxisX * rotationSpeed);
                transform.RotateAround(target.position, Vector3.right, mouseAxisY * rotationSpeed);
            }
        }

        void HandleOnSpawnedObjetUpdate(GameObject obj)
        {
            target = obj.transform;
        }

        private void OnDestroy()
        {
            Preview3DHandler.SpawnedObjectUpdateE -= HandleOnSpawnedObjetUpdate;
            InputManager.LightRotateMouseControlInputE -= HandleLightRotateMouseControlInputE;
        }
    }

}
