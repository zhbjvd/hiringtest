﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Preview3D
{
    public class Item3D : MonoBehaviour
    {
        public Preview3D.PreviewType type;

        public MeshRenderer rendererRef;
        public float extrudeAmount;

        private void Start()
        {
            SetExtrusion(0);

            InputManager.On3DObjectClickedE += HandleOnObjectClicked;
        }

        private void OnDestroy()
        {
            InputManager.On3DObjectClickedE -= HandleOnObjectClicked;
        }

        public void SetExtrusion(float updatedValue)
        {
            rendererRef.material.SetFloat("Amount", updatedValue);
        }

        void HandleOnObjectClicked(GameObject go)
        {
            if(go != null && go.GetInstanceID() == this.gameObject.GetInstanceID())
            {
                SetExtrusion(extrudeAmount);

                CancelInvoke("ResetExtrusion");
                Invoke("ResetExtrusion",1);
            }
        }

        void ResetExtrusion()
        {
            SetExtrusion(0);
        }
    }
}
