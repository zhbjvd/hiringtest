﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Preview3D
{
    public enum PreviewType
    {
        Cube,
        Sphere
    }

    public class Preview3DHandler : MonoBehaviour
    {
        private static Preview3DHandler _instance;
        public static Preview3DHandler Instance
        {
            get
            {
                return _instance;
            }
        }
        public PreviewType currentPreviewType = PreviewType.Cube;

        private GameObject currentSpawnedItem;

        public Light lightRef;

        public List<Item3D> previewList;

        public static Action<GameObject> SpawnedObjectUpdateE;

        private void Awake()
        {
            _instance = this;
        }

        // Use this for initialization
        void Start()
        {
            SpawnItem();
        }

        void SpawnItem()
        {
            var foundItem = previewList.Find(item => item.type == currentPreviewType);
            
            if(currentSpawnedItem != null)
            {
                Destroy(currentSpawnedItem);
            }

            if(foundItem != null)
            {
                currentSpawnedItem = Instantiate(foundItem.gameObject) as GameObject;

                if(SpawnedObjectUpdateE != null)
                {
                    SpawnedObjectUpdateE(currentSpawnedItem);
                }
            }
        }

        public void SetItem(PreviewType newPreviewType)
        {
            if(currentPreviewType != newPreviewType)
            {
                currentPreviewType = newPreviewType;
                SpawnItem();
            }
        }

        public void SetLightIntensity(float updatedValue)
        {
            if (lightRef != null)
            {
                lightRef.intensity = updatedValue;
            }
        }
    }
}

