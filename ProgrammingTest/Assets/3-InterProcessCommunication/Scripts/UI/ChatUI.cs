﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IPC
{
    public class ChatUI : MonoBehaviour
    {
        public Text connectionStateText;

        public RectTransform chatMessageContent;
        public InputField chatMessageArea;
        private List<ChatMessageItemUI> chatMessagesList;

        public ChatMessageItemUI myChatMessagePrefab;
        public ChatMessageItemUI senderChatMessagePrefab;

        Queue<ChatMessage> chatQueue = new Queue<ChatMessage>();

        private void Start()
        {
            ChatSystemManager.Instance.ChatSystem.messageReceivedE += OnChatMessageReceived;
        }
        
        void SetConnectionState()
        {

        }

        public void OnChatMessageReceived(string chatMessage)
        {
            AddChatMessage(new ChatMessage(chatMessage, DateTime.Now, false));
        }

        public void OnConnectionButtonPressed()
        {
            if(ChatSystemManager.Instance.ChatSystem.GetConnectionStatus == ChatConnectionStatus.Disconnected ||
                ChatSystemManager.Instance.ChatSystem.GetConnectionStatus == ChatConnectionStatus.Idle)
            {
                ChatSystemManager.Instance.ChatSystem.Connect();
            }
            else
            {
                Debug.Log("ChatUI : OnDisconnectButtonPressed : Currently connected so ignoring connect call");
            }
        }

        public void OnDisconnectionButtonPressed()
        {
            if (ChatSystemManager.Instance.ChatSystem.GetConnectionStatus == ChatConnectionStatus.Connected ||
                ChatSystemManager.Instance.ChatSystem.GetConnectionStatus == ChatConnectionStatus.WaitingForConnection)
            {
                ChatSystemManager.Instance.ChatSystem.Disconnect();
            }
            else
            {
                Debug.Log("ChatUI : OnDisconnectButtonPressed : Currently not connected so ignoring disconnect call");
            }
            
        }

        private void Update()
        {
            if(chatQueue != null && chatQueue.Count > 0)
            {
                SpawnChatMessage(chatQueue.Dequeue());
            }
            if (ChatSystemManager.Instance != null && ChatSystemManager.Instance.ChatSystem != null)
            {
                connectionStateText.text = ChatSystemManager.Instance.ChatSystem.GetStatus().ToString();
            }
        }

        public void OnSendButtonPressed()
        {
            if(ChatSystemManager.Instance.ChatSystem != null && ChatSystemManager.Instance.ChatSystem.IsConnectedToClient)
            {
                if (!string.IsNullOrEmpty(chatMessageArea.text))
                {
                    AddChatMessage(new ChatMessage(chatMessageArea.text, DateTime.Now, true));

                    ChatSystemManager.Instance.ChatSystem.SendMessage(chatMessageArea.text);

                    chatMessageArea.text = string.Empty;
                }
                else
                {
                    Debug.Log("ChatUI : OnSendButtonPressed : Please write message before sending");
                }
            }
            else
            {
                Debug.Log("ChatUI : OnSendButtonPressed : Currently not connected to any client.");
            }
        }

        public void OnClearButtonPressed()
        {
            Clear();
            chatQueue.Clear();
        }

        void AddChatMessage(ChatMessage chatData)
        {
            chatQueue.Enqueue(chatData);
        }

        void SpawnChatMessage(ChatMessage chatData)
        {
            var chatItem = Instantiate(chatData.isMine ? myChatMessagePrefab : senderChatMessagePrefab);

            if (chatItem != null)
            {
                if (chatMessagesList == null)
                {
                    chatMessagesList = new List<ChatMessageItemUI>();
                }
                chatMessagesList.Add(chatItem);

                chatItem.transform.SetParent(chatMessageContent);
                chatItem.transform.localScale = Vector3.one;
                chatItem.Init(chatData);
            }
        }

        void Clear()
        {
            if (chatMessagesList == null)
            {
                for (int itemIndex = 0; itemIndex < chatMessagesList.Count; itemIndex++)
                {
                    if (chatMessagesList[itemIndex] != null)
                    {
                        DestroyImmediate(chatMessagesList[itemIndex].gameObject);
                    }
                }

                chatMessagesList.Clear();
            }   
        }
    }
}
