﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace IPC
{
    public class ChatMessageItemUI : MonoBehaviour
    {
        public Text chatMessageText;
        public Text timeStampText;

        private ChatMessage chatMessageRef;

        public void Init(ChatMessage chatMessage)
        {
            chatMessageRef = chatMessage;

            chatMessageText.text = chatMessageRef.chatMessage;
            timeStampText.text = chatMessageRef.timeStamp.ToString(); 
        }
    }
}

