﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IPC
{
    public class ChatSystemManager : MonoBehaviour
    {
        private static ChatSystemManager _instance;
        public static ChatSystemManager Instance
        {
            get
            {
                return _instance;
            }
        }
        private ChatSystem chatSystem;

        public ChatSystem ChatSystem
        {
            get
            {
                return chatSystem;
            }
        }

        private void Awake()
        {
            _instance = this;
            chatSystem = new ChatSystem(true);
        }

        private void OnDestroy()
        {
            if (ChatSystem != null)
            {
                ChatSystem.Disconnect();
            }
        }

        private void Update()
        {
            //Debug.Log("ChatSystem.IsConnected : " + ChatSystem.IsConnectedToClient);
        }
    }
}
