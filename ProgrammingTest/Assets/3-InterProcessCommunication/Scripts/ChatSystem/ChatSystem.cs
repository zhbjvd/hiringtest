﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace IPC
{
    public enum ChatSystemType
    {
        TCPIP,
        Pipe
    }

    [Serializable]
    public class ChatSystem
    {
        private ChatSystemType currentChatSystemType = ChatSystemType.TCPIP;
        private bool isServer = true;

        IChatSystemConnection chatClient;

        public bool IsConnectedToClient
        {
            get
            {
                return chatClient != null ? chatClient.IsConnected() : false;
            }
        }

        public ChatConnectionStatus GetConnectionStatus
        {
            get
            {
                if (chatClient != null)
                {
                    return chatClient.ConnectionStatus;
                }
                else
                {
                    return ChatConnectionStatus.Disconnected;
                }
            }
        }
        

        public Action<string> messageReceivedE;

        public ChatSystem(bool isServer)
        {
            this.isServer = isServer;
        }

        public void Connect()
        {
            chatClient = new TCPChatServer();
            chatClient.Connect();

            chatClient.SetOnMessageReceivedDelegate(OnMessageReceived);
        }

        public ChatConnectionStatus GetStatus()
        {
            if(chatClient != null)
            {
                return chatClient.ConnectionStatus;
            }
            else
            {
                return ChatConnectionStatus.Disconnected;
            }
        }

        public void Disconnect()
        {
            if(chatClient != null)
            {
                chatClient.Disconnect();

                chatClient.RemoveOnMessageReceivedDelegate(OnMessageReceived);
            }
        }

        public void SendMessage(string message)
        {
            chatClient.SendMessage(message);
        }

        void OnMessageReceived(string message)
        {
            if(messageReceivedE != null)
            {
                messageReceivedE(message);
            }
        }
            
        
    }
}
