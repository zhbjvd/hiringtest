﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace IPC
{
    public class TCPChatServer : IChatSystemConnection
    {
        ChatConnectionStatus connectionStatus = ChatConnectionStatus.Idle;
        public ChatConnectionStatus ConnectionStatus
        {
            get
            {
                return connectionStatus;
            }
            set
            {
                connectionStatus = value;
            }
        }

        

        #region private members 	
        /// <summary> 	
        /// TCPListener to listen for incomming TCP connection 	
        /// requests. 	
        /// </summary> 	
        private TcpListener tcpListener;
        /// <summary> 
        /// Background thread for TcpServer workload. 	
        /// </summary> 	
        private Thread tcpListenerThread;
        /// <summary> 	
        /// Create handle to connected tcp client. 	
        /// </summary> 	
        private TcpClient connectedTcpClient;

        private Action<string> OnMessageReceivedE;
        private Action<ChatConnectionStatus> OnConnectionStatusE;
        #endregion

        public void Connect()
        {
            // Start TcpServer background thread 		
            tcpListenerThread = new Thread(new ThreadStart(ListenForIncommingRequests));
            tcpListenerThread.IsBackground = true;
            tcpListenerThread.Start();

            SetConnectionStatus(ChatConnectionStatus.WaitingForConnection);
        }

        /// <summary> 	
        /// Runs in background TcpServerThread; Handles incomming TcpClient requests 	
        /// </summary> 	
        private void ListenForIncommingRequests()
        {
            try
            {
                // Create listener on localhost port 8052. 			
                tcpListener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8052);
                tcpListener.Start();
                
                Debug.Log("Server is listening");

                Byte[] bytes = new Byte[1024];

                while (true)
                {
                   using (connectedTcpClient = tcpListener.AcceptTcpClient())
                    {
                        Debug.Log("TCPChatServer : Connected");

                        SetConnectionStatus(ChatConnectionStatus.Connected);

                        // Get a stream object for reading 					
                        using (NetworkStream stream = connectedTcpClient.GetStream())
                        {
                            int length;
                            // Read incomming stream into byte arrary. 						
                            while ((length = stream.Read(bytes, 0, bytes.Length)) != 0)
                            {
                                var incommingData = new byte[length];
                                Array.Copy(bytes, 0, incommingData, 0, length);
                                // Convert byte array to string message. 							
                                string clientMessage = Encoding.ASCII.GetString(incommingData);
                                Debug.Log("client message received as: " + clientMessage);

                                if(clientMessage != null)
                                {
                                    if(OnMessageReceivedE != null)
                                    {
                                        OnMessageReceivedE(clientMessage);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (SocketException socketException)
            {
                Debug.Log("SocketException " + socketException.ToString());
                Disconnect();

                SetConnectionStatus(ChatConnectionStatus.Disconnected);
            }
            catch(Exception e)
            {
                Disconnect();
                Debug.Log("TCPChatServer : ListenForIncommingRequests : Exception : " + e.Message);
            }
        }

        public void Disconnect()
        {
            if(tcpListener != null)
            {
                tcpListener.Stop();
            }
            if (connectedTcpClient != null && connectedTcpClient.Client != null)
            {
                connectedTcpClient.Close();
                connectedTcpClient.Client.Close();
            }

            if (tcpListenerThread != null)
            {
                tcpListenerThread.Abort();
            }

            SetConnectionStatus(ChatConnectionStatus.Disconnected);
        }

        public void SendMessage(string message)
        {
            if (connectedTcpClient == null)
            {
                return;
            }

            try
            {
                // Get a stream object for writing. 			
                NetworkStream stream = connectedTcpClient.GetStream();
                if (stream.CanWrite)
                {
                    // Convert string message to byte array.                 
                    byte[] serverMessageAsByteArray = Encoding.ASCII.GetBytes(message);
                    // Write byte array to socketConnection stream.               
                    stream.Write(serverMessageAsByteArray, 0, serverMessageAsByteArray.Length);

                    Debug.Log("TCPChatServer : SendMessage : " + message);
                }
            }
            catch (SocketException socketException)
            {
                Debug.Log("TCPChatServer : SendMessage  Socket exception: " + socketException);
            }
            catch(Exception e)
            {
                Debug.Log("TCPChatServer : SendMessage : Exception : " + e.Message);
            }
        }

        public void SetOnMessageReceivedDelegate(Action<string> eventReciever)
        {
            OnMessageReceivedE += eventReciever;
        }

        public void RemoveOnMessageReceivedDelegate(Action<string> eventReciever)
        {
            OnMessageReceivedE -= eventReciever;
        }

        public bool IsConnected()
        {
            if (connectedTcpClient != null && connectedTcpClient.Client != null)
            {
                return connectedTcpClient.Client.Connected;
            }
            else
            {
                return false;
            }
        }

        void SetConnectionStatus(ChatConnectionStatus status)
        {
            connectionStatus = status;

            if (OnConnectionStatusE != null)
            {
                OnConnectionStatusE(ConnectionStatus);
            }
        }

        public void SetConnectionStatusUpdateDelegate(Action<ChatConnectionStatus> updatedStatus)
        {
            OnConnectionStatusE += updatedStatus;
        }

        public void RemoveConnectionStatusUpdateDelegate(Action<ChatConnectionStatus> updatedStatus)
        {
            OnConnectionStatusE -= updatedStatus;
        }
    }
}
    
