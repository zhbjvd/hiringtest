﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Text;
using System.Threading;
using UnityEngine;

namespace IPC
{
    public class PipeChatServer : IChatSystemConnection
    {
        private ChatConnectionStatus chatConnectionState = ChatConnectionStatus.Idle;

        public ChatConnectionStatus ConnectionStatus
        {
            get
            {
                return chatConnectionState;
            }
        }
        NamedPipeServerStream pipeServer;

        Action<ChatConnectionStatus> OnConnectionStateUpdateE;
        Thread server;



        public void Connect()
        {
            SetConnectionState(ChatConnectionStatus.Connecting);
            pipeServer = new NamedPipeServerStream("testpipe", PipeDirection.InOut, 1);

            server = new Thread(ServerThread);
            server.Start();
        }

        StreamString streamRef;
        private void ServerThread(object data)
        {
            SetConnectionState(ChatConnectionStatus.WaitingForConnection);

            // Wait for a client to connect
            pipeServer.WaitForConnection();

            while (true)
            {
                try
                {
                    SetConnectionState(ChatConnectionStatus.Connected);
                    // Read the request from the client. Once the client has
                    // written to the pipe its security token will be available.

                    streamRef = new StreamString(pipeServer);

                    string filename = streamRef.ReadString();

                    if (filename != null)
                        Debug.Log("FileName : " + filename);

                }
                // Catch the IOException that is raised if the pipe is broken
                // or disconnected.
                catch (IOException e)
                {
                    SetConnectionState(ChatConnectionStatus.Disconnected);
                    Debug.LogError("ERROR: {0}" + e.Message);
                    pipeServer.Close();
                }
                catch (Exception e)
                {
                    SetConnectionState(ChatConnectionStatus.Disconnected);
                    Debug.Log("PipeChatServer : ServerThread : Exception : " + e.Message + " : Stack : " + e.StackTrace);
                    pipeServer.Close();
                }
            }
        }

        public void Disconnect()
        {
            if (pipeServer != null)
            {
                if (pipeServer.IsConnected)
                {
                    pipeServer.Disconnect();
                }

                pipeServer.Dispose();
                pipeServer = null;
            }

            if (server != null)
            {
                server.Abort();
                server = null;
            }
        }

        public bool IsConnected()
        {
            return pipeServer.IsConnected;
        }

        public void RemoveConnectionStatusUpdateDelegate(Action<ChatConnectionStatus> updatedStatus)
        {
            OnConnectionStateUpdateE -= updatedStatus;
        }

        public void RemoveOnMessageReceivedDelegate(Action<string> eventReciever)
        {
            //throw new NotImplementedException();
        }

        public void SendMessage(string message)
        {
            //if(streamRef != null)
            //{
            //    streamRef.WriteString(message);
            //}
        }

        public void SetConnectionStatusUpdateDelegate(Action<ChatConnectionStatus> updatedStatus)
        {
            OnConnectionStateUpdateE += updatedStatus;
        }

        public void SetOnMessageReceivedDelegate(Action<string> eventReciever)
        {

        }

        void SetConnectionState(ChatConnectionStatus status)
        {
            chatConnectionState = status;

            if (OnConnectionStateUpdateE != null)
            {
                OnConnectionStateUpdateE(status);
            }
        }

        public class StreamString
        {
            private Stream ioStream;
            private UnicodeEncoding streamEncoding;

            public StreamString(Stream ioStream)
            {
                this.ioStream = ioStream;
                streamEncoding = new UnicodeEncoding();
            }

            public string ReadString()
            {
                int len;
                len = ioStream.ReadByte() * 256;
                len += ioStream.ReadByte();
                byte[] inBuffer = new byte[len];
                ioStream.Read(inBuffer, 0, len);

                string outString = streamEncoding.GetString(inBuffer);

                //ioStream.Flush();

                return outString;


            }

            public int WriteString(string outString)
            {
                byte[] outBuffer = streamEncoding.GetBytes(outString);
                int len = outBuffer.Length;
                if (len > UInt16.MaxValue)
                {
                    len = (int)UInt16.MaxValue;
                }
                ioStream.WriteByte((byte)(len / 256));
                ioStream.WriteByte((byte)(len & 255));
                ioStream.Write(outBuffer, 0, len);
                ioStream.Flush();

                return outBuffer.Length + 2;
            }


        }
    }
}
