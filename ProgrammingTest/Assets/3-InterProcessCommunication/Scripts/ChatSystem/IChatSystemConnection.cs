﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IPC
{
    public enum ChatConnectionStatus
    {
        Idle,
        Connecting,
        Connected,
        WaitingForConnection,
        Disconnecting,
        Disconnected
    }
    public interface IChatSystemConnection
    {
        bool IsConnected();
        void Connect();
        void Disconnect();
        void SendMessage(string message);

        void SetOnMessageReceivedDelegate(System.Action<string> eventReciever);
        void RemoveOnMessageReceivedDelegate(System.Action<string> eventReciever);
        void SetConnectionStatusUpdateDelegate(System.Action<ChatConnectionStatus> updatedStatus);
        void RemoveConnectionStatusUpdateDelegate(System.Action<ChatConnectionStatus> updatedStatus);

        ChatConnectionStatus ConnectionStatus
        {
            get;
        }
    }
}
