﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace IPC
{
    [Serializable]
    public class ChatMessage
    {
        public string chatMessage;
        public DateTime timeStamp;

        public bool isMine = false;

        public ChatMessage(string message, DateTime time, bool isMine)
        {
            chatMessage = message;
            timeStamp = time;
            this.isMine = isMine;
        }
    }
}
