﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

namespace TextureSubtraction
{
    [XmlRoot("TexturesConfig")]
    public class TexturesConfig
    {
        [XmlElement("Texture1Path")]
        public string Texture1Path;

        [XmlElement("Texture2Path")]
        public string Texture2Path;

        public override string ToString()
        {
            return string.Format("TextureConfig : Texture1Path : {0} : Texture2Path : {1}", Texture1Path, Texture2Path);
        }
    }

    public class XMLLoader : MonoBehaviour
    {
        public TextAsset xmlFile;

        public TexturesConfig Load()
        {
            try
            {
                var serializer = new XmlSerializer(typeof(TexturesConfig));
                return serializer.Deserialize(new StringReader(xmlFile.text)) as TexturesConfig;
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogError("Exception loading config file: " + e);

                return null;
            }
        }
    }

}
