﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace TextureSubtraction
{
    public class TextureSubtractionHandler : MonoBehaviour
    {
        public XMLLoader xmlLoader;

        private TexturesConfig config;

        public Material texture1Mat;
        public Material texture2Mat;

        public Material outputDisplayMat;

        public Material outputMat;

        public RenderTexture RT;

        Texture2D textureA;
        Texture2D textureB;

        // Use this for initialization
        void Start()
        {
            if (xmlLoader != null)
            {
                config = xmlLoader.Load();
            }

            if(config == null)
            {
                Debug.LogError("TextureSubtractionHandler : Start : TextureConfig is null");
            }
            else
            {
                Debug.Log("TextureSubtractionHandler : Start : TextureConfig : " + config.ToString());
            }
            textureA = LoadTextureAtPath(config.Texture1Path);
            textureB = LoadTextureAtPath(config.Texture2Path);

            if(textureA != null && textureB != null)
            {
                texture1Mat.SetTexture("_MainTex", textureA);
                texture2Mat.SetTexture("_MainTex", textureB);

                outputMat.SetTexture("_MainTex", textureA);
                outputMat.SetTexture("_MainTex2", textureB);
            }
            else
            {
                Debug.LogError("TextureSubtraction : Unable to load Textures");
            }
        }

        public Texture2D LoadTextureAtPath(string path)
        {
            try
            {
                byte[] fileData = File.ReadAllBytes(path);
                Texture2D fullImageTexture = new Texture2D(1, 1);
                fullImageTexture.LoadImage(fileData);
                
                return fullImageTexture;
            }
            catch(Exception e)
            {
                Debug.Log("TextureSubtractionHandler : LoadTextureAtPath : Path : " + path + " : Exception : " + e.Message);

                return null;
            }
        }

        [ContextMenu("Subtract")]
        public void OnSubtractButtonPressed()
        {
            if(textureA != null && textureB != null)
            {
                Graphics.Blit(null, RT, outputMat);

                var outputTexture = RenderTexToTexture2D(RT);

                outputDisplayMat.SetTexture("_MainTex", outputTexture);

                outputTexture.name = "Output.png";
                SaveImageLocally(Path.Combine(Application.persistentDataPath, outputTexture.name), outputTexture);
            }
            else
            {
                Debug.LogError("TextureSubtractionHandler : OnSubtractionButtonPressed : Cannot continue : Textures not available");
            }
            
        }

        static Texture2D RenderTexToTexture2D(RenderTexture renderTexture)
        {
            if (renderTexture == null)
                return null;

            Texture2D texture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.ARGB32, false);
            if (texture == null)
                return null;

            RenderTexture.active = renderTexture;
            texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            texture.Apply();

            return texture;
        }

        public static bool SaveImageLocally(string path, Texture2D image)
        {
            try
            {
                Debug.Log("TextureSubtractionHandler : SaveImageLocally At Path : " + path);

                byte[] bytes = image.EncodeToJPG();

                File.WriteAllBytes(path, bytes);
            }
            catch (Exception e)
            {
                Debug.LogWarning("TextureSubtractionHandler : SaveImageLocally : Path : " + path + ": Exception : " + e.Message);
            }

            return false;
        }
    }
}
