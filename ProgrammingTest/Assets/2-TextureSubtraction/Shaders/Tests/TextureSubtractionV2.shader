﻿ Shader "Custom/TextureSubtractionV2" {
         Properties 
         {
             _MainTex ("Texture 1", 2D) = "white" {}
             _MainTex2 ("Texture 2", 2D) = "white" {}
         }
         SubShader 
         {
             Tags { "RenderType"="Opaque" }
            
             CGPROGRAM
             // Physically based Standard lighting model, and enable shadows on all light types
             #pragma surface surf Standard
      
             // Use shader model 3.0 target, to get nicer looking lighting
             #pragma target 3.0
      
             sampler2D _MainTex;
             sampler2D _MainTex2;
      
             struct Input 
             {
                 float2 uv_MainTex;
                 float2 uv_MainTex2;
             };
      
             void surf (Input IN, inout SurfaceOutputStandard o) {
                 // Albedo comes from a texture tinted by color

                 fixed4 c = tex2D (_MainTex2, IN.uv_MainTex2) - tex2D (_MainTex, IN.uv_MainTex) ;

                 o.Albedo = c.rgb;

             }
             ENDCG
         }
         FallBack "Diffuse"
     }